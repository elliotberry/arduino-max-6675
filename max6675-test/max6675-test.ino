


#include <Wire.h>

// Sample Arduino MAX6675 Arduino Sketch

#include "max6675.h"

int ktcSO = 8;
int ktcCS = 9;
int ktcCLK = 10;

MAX6675 ktc(ktcCLK, ktcCS, ktcSO);



void setup() {


  Serial.begin(9600);

}

void loop() {

  Serial.print(ktc.readFahrenheit(), 2);
  Serial.print(" F / ");


}
